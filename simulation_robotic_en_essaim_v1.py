## importation
from tkinter import*
from tkinter import ttk
import time
import random

### progamme pour un robot


def programe(arene,robot):




    if lire_mode(robot)=="recherche":

        if lire_stockage(robot)==[]:
            x=(random.random()*2-1)
            y=(random.random()*2-1)##on peut aller plus vite
            stocker(robot,[x,y])



        vecteur=lire_stockage(robot)
        if mouvement_est_possible(arene,robot,vecteur):
                avancer(arene,robot,vecteur)

        else:
            x=(random.random()*2-1)
            y=(random.random()*2-1)##on peut aller plus vite
            stocker(robot,[x,y])


        if len(lire_messages(robot))>0:
            if 'Cible' in lire_messages(robot)[0]:
                stocker(robot,[lire_stockage(robot),lire_messages(robot)[0][-1]])
                passer_en_mode(robot,"messager")

            elif 'go' in lire_messages(robot)[0]:
                stocker(robot,lire_messages(robot)[0][-1])
                passer_en_mode(robot,"attaque")
            effacer_messages(robot)



    if lire_mode(robot)=="messager":
        #print(lire_stockage(robot))
        vecteur=lire_stockage(robot)[0]
        if mouvement_est_possible(arene,robot,vecteur):
            avancer(arene,robot,vecteur)
        else:
            x=(random.random()*2-1)
            y=(random.random()*2-1)##on peut aller plus vite
            stocker(robot,[[x,y],lire_stockage(robot)[-1]])


        effacer_messages(robot)
        #print("____",lire_stockage(robot))
        envoyer_message(arene,robot,['go',lire_stockage(robot)[-1]])

    if lire_mode(robot)=="attaque":
        #print([lire_stockage(robot)[0]-coord_actuel(robot)[0],lire_stockage(robot)[1]-coord_actuel(robot)[1]])
        vecteur=[lire_stockage(robot)[0]-coord_actuel(robot)[0],lire_stockage(robot)[1]-coord_actuel(robot)[1]]
        avancer(arene,robot,vecteur)
        effacer_messages(robot)


#tester message aussi il faut tout tester






#########################################################################################
#########################################################################################





      ###        ###      ###     ###          ###     ###        ###        ###
      ###        ###       ###     ###        ###     ###         ###        ###
      ###        ###        ###     ###      ###     ###          ###        ###
      ###        ###         ###     ###    ###     ###           ###        ###
      ###        ###          ###     ###  ###     ###            ###        ###
      ###        ###           ###     ######     ###             ###        ###
      ###        ###            ###     ######   ###              ###        ###
        ###    ###               ###   ### ###  ###                 ###    ###
          ######                  ######    ######                    ######





#########################################################################################
#########################################################################################




#########################################################################################
#########################################################################################




###   Pas besoin de lire le programme de la simulation  ###




#########################################################################################
#########################################################################################


#########################################################################################
#########################################################################################




###   Ne pas aller plus bas  ###



#########################################################################################
#########################################################################################






#########################################################################################
#########################################################################################


#########################################################################################
#########################################################################################




      ###        ###      ###     ###          ###     ###        ###        ###
      ###        ###       ###     ###        ###     ###         ###        ###
      ###        ###        ###     ###      ###     ###          ###        ###
      ###        ###         ###     ###    ###     ###           ###        ###
      ###        ###          ###     ###  ###     ###            ###        ###
      ###        ###           ###     ######     ###             ###        ###
      ###        ###            ###     ######   ###              ###        ###
        ###    ###               ###   ### ###  ###                 ###    ###
          ######                  ######    ######                    ######





#########################################################################################
#########################################################################################



### programme de la simulation

## objet
class objet_robot:
    def __init__(self,coord,id):
        self.id=id
        self.coord=coord
        self.mode="recherche"#recherche_attaque_communication
        self.message=[]
        self.stockage=[]
        self.choc=False
        self.action=True


class objet_arene:
    def __init__(self,liste_coord,coord_cible):
        self.score=0
        self.temps=0
        self.liste_robots=[]
        for i in range(len(liste_coord)):
            self.liste_robots.append(objet_robot(liste_coord[i],i))
        self.cible=coord_cible



## fonction variable

def rayon_action():
    return(rayon_prise_au_sol()*4)


def score_cibe():
    return(100000)

def rayon_cible():
    return(10)

def rayon_prise_au_sol():
    return(2)

def dimension():
 return [300,300]

def pause():
    return(0.01)

## fonction utilisateur

def avancer(arene,robot,vecteur):
    if robot.action:
        #verification de la norme est bien inferieur à 1
        norme=norme2(vecteur)
        if norme>1:
            vecteur[0]*=1/norme
            vecteur[1]*=1/norme
        new_coord=[robot.coord[0]+vecteur[0],robot.coord[1]+vecteur[1]]
        #verification de la colision avec les murs
        #sur x
        if new_coord[0]>dimension()[0]:
            new_coord[0]=dimension()[0]
            robot.choc=True
        elif new_coord[0]<0:
            new_coord[0]=0
            robot.choc=True
        else:
            robot.choc=False
        #sur y
        if new_coord[1]>dimension()[1]:
            new_coord[1]=dimension()[1]
            robot.choc=True
        elif new_coord[1]<0:
            new_coord[1]=0
            robot.choc=True

        for i in arene.liste_robots:
            if i.id!=robot.id:
                distance=norme2([i.coord[0]-new_coord[0],i.coord[1]-new_coord[1]])
                if distance<rayon_prise_au_sol():
                    new_coord=robot.coord
                    robot.choc=True
                    break
        robot.coord=new_coord
        robot.action=False


def mouvement_est_possible(arene,robot,vecteur):
        #verification de la norme est bien inferieur à 1
        norme=norme2(vecteur)
        if norme>1:
            vecteur[0]*=1/norme
            vecteur[1]*=1/norme
        new_coord=[robot.coord[0]+vecteur[0],robot.coord[1]+vecteur[1]]

        save_coord=[robot.coord[0]+vecteur[0],robot.coord[1]+vecteur[1]]

        #verification de la colision avec les murs
        #sur x
        if new_coord[0]>dimension()[0]:
            new_coord[0]=dimension()[0]
        elif new_coord[0]<0:
            new_coord[0]=0

        #sur y
        if new_coord[1]>dimension()[1]:
            new_coord[1]=dimension()[1]
            robot.choc=True
        elif new_coord[1]<0:
            new_coord[1]=0


        for i in arene.liste_robots:
            if i.id!=robot.id:
                distance=norme2([i.coord[0]-new_coord[0],i.coord[1]-new_coord[1]])
                if distance<rayon_prise_au_sol():
                    new_coord=robot.coord

                    break
        if save_coord==new_coord:
            return(True)
        else:
            return(False)


'''
def obstacle_dans_la_direction(arene,robot,vecteur):#utiliser la projection
    distance=max(dimension()[0],dimension()[1])
    for i in arene.liste_robots:
        if i.id!=robot.id:
            vect1=[i.coord[0]-robot.coord[0],i.coord[1]-robot.coord[1]]
            vect_proj=projection(vect1,vecteur)
            vect_orto=[vect1[0]-vect_proj[0],vect1[1]-vect_proj[1]]
            if norme2(vect_orto)<=rayon_prise_au_sol():
                distance=min(distance,norme2(vect_proj))
    if vecteur[0]>0:
        distance=min(distance,dimension()[0]-robot.coord[0]/vecteur[0])

    if vecteur[1]>0:
        distance=min(distance,dimension()[1]-robot.coord[1]/vecteur[1])
    if vecteur[0]<0:
        distance=min(distance,0-robot.coord[0]/vecteur[0])

    if vecteur[1]<0:
        distance=min(distance,0-robot.coord[1]/vecteur[1])
    return(distance)
'''



def coord_actuel(robot):#on peut faire mieux
    return(robot.coord)

def envoyer_message(arene,robot,info):
    c=0
    for i in arene.liste_robots:
        if i.id!=robot.id:
            distance=norme2([i.coord[0]-robot.coord[0],i.coord[1]-robot.coord[1]])
            if distance<rayon_action():
                i.message.append(info)
                c+=1
    return(c)#penser qu'il y a un retour



def lire_messages(robot):
    return(robot.message)


def effacer_messages(robot):
    robot.message=[]

def stocker(robot,info):
    robot.stockage=info

def lire_stockage(robot):
    return(robot.stockage)

def passer_en_mode(robot,mode):
    robot.mode=mode


def lire_mode(robot):
    return(robot.mode)

##fonction de fonctionnement





def norme2(vecteur):
    return((vecteur[0]**2+vecteur[1]**2)**0.5)

def projection(vect1,vect2):
    scalaire=vect1[0]*vect2[0]+vect1[1]*vect2[1]
    vect=[0,0]
    vect[0]=vect1[0]*scalaire/norme2(vect2)
    vect[1]=vect1[1]*scalaire/norme2(vect2)
    return(vect)

def init():
    liste=[]
    for i in range(4):
        for j in range(3):
                liste.append([i*3.5*rayon_prise_au_sol()+5,j*3.5*rayon_prise_au_sol()+5])


    arene=objet_arene(liste,[200,200])
    fen,dess,barre=init_affichage(arene)
    actualiser_affichage(fen,dess,arene,barre)
    une_seconde(fen,dess,arene,barre)
    fen.mainloop()


def une_seconde(fen,dess,arene,barre):#penser à reinitialiser les actions
    arene.temps+=1
    for i in arene.liste_robots:
        programe(arene,i)###############################dans actualiser il faut prevoir les chocs
        arene.score+=compter_le_score(arene)
        i.action=True

    actualiser_affichage(fen,dess,arene,barre)
    time.sleep(pause())
    print("temps : ",arene.temps)

    if arene.score>=score_cibe():
        print("score===",arene.score,"  temps : ",arene.temps)
        return(arene.temps)
    dess.after(1,une_seconde,fen,dess,arene,barre)


def init_affichage(arene):
    fen= Tk()
    fen.title('Robotarium')
    fen.minsize(width=800,height=800)
    fen.resizable(width=YES,height=YES)

    barre=ttk.Progressbar(orient="vertical",length=500)
    barre.place(x=750,y=10)




    dess=Canvas(fen,width=700,height=700,bg='white',bd=5,relief='ridge')
    dess.place(x=10,y=10)#prevoir de faire un affcihage variable avec une variable globale
    #prevoir aussir que dess prenne la forme de l'arenne mais avec la taille max dans la fenetre
    arene.dessin_robot=[]
    arene.dessin_rayon=[]
    arene.dessin_cible=[]

    arene.dessin_cible.append(dess.create_oval((arene.cible[0]-rayon_cible())*700/dimension()[0],(arene.cible[1]-rayon_cible())*700/dimension()[1],(arene.cible[0]+rayon_cible())*700/dimension()[0],(arene.cible[1]+rayon_cible())*700/dimension()[1],fill='red'))#on peut faire mieux ici

    for i in arene.liste_robots:
        arene.dessin_rayon.append(dess.create_oval((i.coord[0]-rayon_action())*700/dimension()[0],(i.coord[1]-rayon_action())*700/dimension()[1],(i.coord[0]+rayon_action())*700/dimension()[0],(i.coord[1]+rayon_action())*700/dimension()[1],fill='skyblue'))#on peut faire mieux ici


    for i in arene.liste_robots:

        arene.dessin_robot.append(dess.create_oval((i.coord[0]-rayon_prise_au_sol())*700/dimension()[0],(i.coord[1]-rayon_prise_au_sol())*700/dimension()[1],(i.coord[0]+rayon_prise_au_sol())*700/dimension()[0],(i.coord[1]+rayon_prise_au_sol())*700/dimension()[1],fill='black'))#on peut faire mieux ici

    return(fen,dess,barre)



def actualiser_affichage(fen,dess,arene,barre):

    for i in range(len(arene.dessin_robot)):
        dess.coords(arene.dessin_rayon[i],(arene.liste_robots[i].coord[0]-rayon_action())*700/dimension()[0],(arene.liste_robots[i].coord[1]-rayon_action())*700/dimension()[1],(arene.liste_robots[i].coord[0]+rayon_action())*700/dimension()[0],(arene.liste_robots[i].coord[1]+rayon_action())*700/dimension()[1])

    for i in range(len(arene.dessin_robot)):
        dess.coords(arene.dessin_robot[i],(arene.liste_robots[i].coord[0]-rayon_prise_au_sol())*700/dimension()[0],(arene.liste_robots[i].coord[1]-rayon_prise_au_sol())*700/dimension()[1],(arene.liste_robots[i].coord[0]+rayon_prise_au_sol())*700/dimension()[0],(arene.liste_robots[i].coord[1]+rayon_prise_au_sol())*700/dimension()[1])

    barre["value"]=arene.score/score_cibe()*100


def compter_le_score(arene): #penser à envyer un message de l'attaque
    new_score=0
    for i in arene.liste_robots:
        distance=norme2([i.coord[0]-arene.cible[0],i.coord[1]-arene.cible[1]])
        if distance<=rayon_cible():
            new_score+=1
            i.message.append(["Cible",arene.cible])

    return(new_score)

## programme

init()