
# Projet : Robotique en essaim

## Introduction

Le but de ce petit projet est d'organiser une compétition pour se familiariser avec la robotique en essaim. Pour cela nous utilisons une simulation qui représente 12 robots qui ont pour taches de trouver une cible puis de l'attaquer. Plus il y a de robots sur la cible plus l'attaque est rapide. Le but est donc de trouver et d'attaquer la cible en un minimum de temps.

Votre tache, si vous l'acceptez, est de rédiger un programme pour diriger les robots. J'organiserai une compétition entre tous les programmes et celui qui aura le programme avec le meilleur temps remportera la compétition.

## Comment rédiger mon programme

Pour rédiger votre programme vous devez savoir que le programme est le même dans chaque robot. 
Le but du jeu est de combattre la cible en un minimum de temps. Les robot peuvent envoyer et recevoir des messages. Le rayon de communication est en bleu et le robot est en noir. 
La cible est en rouge. 

Le robot a une mémoire interne pour stocker ce que vous voulez. 
Plus il y a de robots sur la cible et plus l'attaque est efficace. Si le robot passe sur la cible il reçoit un message avec les coordonnées sous la forme `["Cible",coordonnées]`. Vous ne pouvez programmer les coordonnées de la cible à l'avance dans les robots, il faut qu'au moins un robot la trouve pour en connaitre les coordonnées (et ensuite les transmettre par message). Pour éviter les triches je me réserve le droit de modifier la position de la cible. 

Pour rédiger votre programme vous devrez éditer la fonction programme mais vous ne pouvez utiliser que les fonctions suivantes :

```python
avancer(arene,robot,vecteur)
```

Cette fonction permet de dire au robot d'avancer d'un `vecteur` que vous indiquerez avec une liste de deux float. La norme du `vecteur` ne peut pas dépasser 1. Si c'est le cas, il sera normalisé pour avoir une norme de 1. 
Les arguments `arene` et `robot` sont indispensables, il vous suffit de mettre ces variables pour que ça fonctionne (cela permet de dire de faire avancer le robot dans l'arène). La fonction n'a pas de valeur de retour.

```python
mouvement_est_possible(arene,robot,vecteur)
```

Cette fonction a les même arguments que la fonction `avancer`, elle permet de tester si votre robot ne va pas se prendre un mur. 
Elle renvoie `True` si le mouvement est possible et `False` sinon.

```
coord_actuel(robot)
```

Cette fonction a pour valeur de retour les coordonnées du robot. Encore une fois il ne faut pas oublier d'écrire `robot` en argument.

```
lire_messages(robot)
```

Un robot peut recevoir des messages, par exemple quand il est sur la cible. Les messages sont stockés dans une liste. La liste peut être vide ou contenir n'importe quel élément comme une autre liste, une chaine de caractères, un float... Cette fonction retourne simplement la liste des messages en mémoire.

```
effacer_messages(robot)
```


Cette fonction permet de vider la liste des messages. Encore une fois il ne faut pas oublier d'écrire `robot` en argument.

```
stocker(robot,info)
```

Le robot a la capacité de stocker des données, il s'agit d'une variable qui peut stocker n'importe quoi comme une liste, un float... Il faut mettre `robot` ainsi que les infos que vous voulez stocker en argument. ATTENTION, quand vous stockez une variable, vous écrasez le message qui y est stocké, donc il ne faut pas oublier de lire avant d'écrire une info. Je vous conseille d'utiliser une liste.

```
lire_stockage(robot)
```

Cette fonction sert à lire ce que vous avez stocké dans la mémoire, elle retourne ce que vous y avez stocké. Si vous n'avez rien stocké il retourne une liste vide.

```
passer_en_mode(robot,mode)
```

Le robot est par défaut en mode `"recherche"`, vous pouvez le faire passer dans le mode du nom de votre choix avec cette fonction. Le mode est une manière de gérer votre programme, par exemple quand vous passez sur la cible vous pouvez passer en mode attaque. Vous pouvez créer n'importe quel mode, il s'agit simplement d'une chaine de caractère.

```
lire_mode(robot)
```

Permet de connaitre le mode du robot, il retourne simlement la chaine de caractère du mode. Avec un `if` vous pouvez ainsi structurer votre programme.

## Autres informations

Le programme peut utiliser n'importe quel module, il vous suffit de l'importer, vous pouvez créer des fonctions... Le robot exécute simplement la fonction programme en boucle, il vous faut donc simplement écrire votre algorithme dedans (c'est du python...).


Dans la fonction programme il y a un exemple de programme simple. Cet exemple vous permet de voir comment utiliser les fonctions.

Le programme exemple fait chercher la cible aux robots avec une trajectoire aléatoire. Si un robot trouve la cible, il devient messager et transmet le coordonnées de la cible à tous les robots qu'il croise. Si un robot reçoit l'adresse de la cible alors il l'attaque directement.

Il y a une vidéo de cette exemple disponible dans les fichiers, vous pouvez ainsi constater que ce n'est pas la stratégie la plus efficace.

Pour écrire le programme je vous suggère de télécharger le code python. 
Vous pourrez l'éditer et le tester sur votre ordinateur.

Une fois que vous aurez un résultat satisfaisant, vous devez le mettre en ligne. Si vous avez un compte GitLab, allez sur le projet, dans le menu à droite vous pouvez créer une nouvelle branche, et éditer avec votre version de votre fichier. Pensez à bien nommez votre branche avec votre nom et si vous le voulez le nom de votre programme. Vous pouvez créer autant de branche que vous le souhaitez. 
Si vous n'avez pas de compte, vous pouvez en créer un ou me demander de mettre en ligne votre version pour vous.

Une fois en ligne contactez moi sur Messenger, et j'organiserai la compétition quand j'aurai suffisamment de participations.

## Amélioration de la simulation:


Pour les plus motivés d'entre vous on peut chercher à améliorer la simulation.

Pour cela encore une fois contactez-moi puis créez une branche avec un nom du type `simulation_version_"votre nom"`.

Les pistes d'amélioration sont :

- Animer les couleurs en cas de collision (les collisions sont déjà codées mais pas les couleurs).
- Introduire la possibilité d'avoir plusieurs cibles.
- Améliorer l'adaptation de la fenêtre à la taille de l'arène, et si vous êtes chaud, faire en sorte que ca s'adapte à la taille de l'écran.
- Créer des fonctions utiles pour les robots et maitriser un essaim de plus en plus complexe.
- Introduire des obstacles/murs.
- Animer la fin de la simulation en affichant le temps de l'attaque.
- Vous pouvez pousser ce programme pour faire simuler d'autres problèmes que la recherche et l'attaque par exemple la mise en formation (par exemple demander au robot de se mettre en forme d'étoile, ou autre forme complexes), (pour cette idée le code des robots peut rester le même, ce qu'il faut c'est simplement créer une nouvelle condition de fin).
- Rendre la simulation plus jolie (animations, commentaires...).
- Mettre la simulation à rude épreuve avec des dimensions énormes (1000 robots dans une arène énorme).

Je reste à votre disposition pour plus d'infos ou si vous voulez discuter.

## Références :

L'idée de la compétition me vient de cette vidée, mais je vous demande de la regarder seulement si vous êtes à court d'idées, pour que vous veniez avec des programmes originaux et variés : 
https://www.youtube.com/watch?v=5CaVhGTG8eA&t=840s



